// motMystere.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <string>
#include <ctime> // Obligatoire pour rendre des chiffres aléatoires
#include <cstdlib> // IDEM
using namespace std;

string meltingWord(string word) {

    string meltedWord;
    int randomIndex(0);

    while (word.size() != 0)
    {
        randomIndex = rand() % word.size();// on génére un index aléatoire par rapport à la longueur du mot

        meltedWord += word[randomIndex]; // on ajoute la lettre random au mot mélangé

        word.erase(randomIndex, 1); // on enlève la lettre choisie du mot originel

    }

    
    return meltedWord;
}

int main()
{
    srand(time(0)); // initialisation génération nbres aléatoires
    string word,meltedWord, guessedWord;
    cout << "Bienvenue dans le jeu du mot mystere " << endl ;
    cout << " Saisissez un mot :  " << endl;

    cin >> word ;

    meltedWord = meltingWord(word);
    do
    {

    

    cout << "Le mot mystere est : " << meltedWord << endl

    << "Saurez vous trouver ce que c est ?" << endl;

    cin >> guessedWord;

    if (guessedWord == word)
    {
        cout << "Bravo cetait bien " << word << endl;
    }
    else {
        cout << "Et non dommage ! Essaie encore !" << endl << endl << endl;
    }
    } while (guessedWord != word);
    
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
